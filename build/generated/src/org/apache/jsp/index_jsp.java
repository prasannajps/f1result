package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        \n");
      out.write("        \n");
      out.write("        <script>\n");
      out.write("            \n");
      out.write("             var dara = JSON.stringify({\n");
      out.write("            details: {\n");
      out.write("                //  Name: name,\n");
      out.write("                //   Email: email,\n");
      out.write("                // Password:password\n");
      out.write("                url: \"friendprofile\"\n");
      out.write("                 \n");
      out.write("            }\n");
      out.write("        });\n");
      out.write("    //  alert(dara);\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("        $.ajax({\n");
      out.write("// url: \"https://og321fx44a.execute-api.ap-southeast-1.amazonaws.com/sdst/user\",\n");
      out.write("            url: \"f1results\",\n");
      out.write("            data: dara,\n");
      out.write("            crossDomain: true,\n");
      out.write("            type: 'POST',\n");
      out.write("            // dataType: 'json',\n");
      out.write("            'Accept': 'application/json',\n");
      out.write("            'Content-Type': 'application/json',\n");
      out.write("            async: false,\n");
      out.write("            success: function (data) {\n");
      out.write("                var tt = JSON.stringify(data,\n");
      out.write("                        null, 2)\n");
      out.write("                 //alert(tt);\n");
      out.write("                var obj = JSON.parse(tt);\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("                //  alert(JSON.stringify(JSON.parse(JSON.stringify(obj1.Attributes))));\n");
      out.write("                                   //alert(share);\n");
      out.write("                    \n");
      out.write(" \n");
      out.write("                 \n");
      out.write("                \n");
      out.write("\n");
      out.write("            },\n");
      out.write("            error: function (jqXHR, exception) {\n");
      out.write("                var msg = '';\n");
      out.write("                if (jqXHR.status === 0) {\n");
      out.write("                    msg = 'Not connect.\\n Verify Network.';\n");
      out.write("                } else if (jqXHR.status == 404) {\n");
      out.write("                    msg = 'Requested page not found. [404]';\n");
      out.write("                } else if (jqXHR.status == 500) {\n");
      out.write("                    msg = 'Internal Server Error [500].';\n");
      out.write("                } else if (exception === 'parsererror') {\n");
      out.write("                    msg = 'Requested JSON parse failed.';\n");
      out.write("                } else if (exception === 'timeout') {\n");
      out.write("                    msg = 'Time out error.';\n");
      out.write("                } else if (exception === 'abort') {\n");
      out.write("                    msg = 'Ajax request aborted.';\n");
      out.write("                } else {\n");
      out.write("                    msg = 'Uncaught Error.\\n' + jqXHR.responseText;\n");
      out.write("                }\n");
      out.write("                alert(msg);\n");
      out.write("            },beforeSend: function() { $('#ajaxloader').show(); },\n");
      out.write("                complete: function() { $('#ajaxloader').hide(); }\n");
      out.write("        });\n");
      out.write("\n");
      out.write("        </script>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
