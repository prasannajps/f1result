<%-- 

    Document   : index
    Created on : 30-Oct-2018, 16:03:16
    Author     : prasanna
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <link rel="stylesheet" type="text/css" media="screen" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" />
    </head>
    <body>      
        <div>
        <select id="resultsmenu" onchange="f1results()" > 
            <option value="https://www.f1-fansite.com/f1-results/2018-f1-championship-standings/" selected="">2018 F1 Championship Standings</option>
            <option value="https://www.f1-fansite.com/f1-results/2017-f1-championship-standings/">2017 F1 Championship Standings</option>
            <option value="https://www.f1-fansite.com/f1-results/2016-f1-championship-standings/">2016 F1 Championship Standings</option>
            <option value="https://www.f1-fansite.com/f1-results/2015-f1-championship-standings/">2015 F1 Championship Standings</option>
            <option value="https://www.f1-fansite.com/f1-results/2014-f1-championship-standings/">2014 F1 Championship Standings</option>
            <option value="https://www.f1-fansite.com/f1-results/2013-f1-championship-standings/">2013 F1 Championship Standings</option>
            <option value="https://www.f1-fansite.com/f1-results/2012-f1-championship-standings/">2012 F1 Championship Standings</option>
            <option value="https://www.f1-fansite.com/f1-results/2011-f1-championship-standings/">2011 F1 Championship Standings</option>
            <option value="https://www.f1-fansite.com/f1-results/2010-f1-championship-standings/">2010 F1 Championship Standings</option>
            <option value="https://www.f1-fansite.com/f1-results/2009-f1-championship-standings/">2009 F1 Championship Standings</option>
            <option value="https://www.f1-fansite.com/f1-results/2008-f1-championship-standings/">2008 F1 Championship Standings</option>
            <option value="https://www.f1-fansite.com/f1-results/2007-f1-championship-standings/">2007 F1 Championship Standings</option>
            <option value="https://www.f1-fansite.com/f1-results/2006-f1-championship-standings/">2006 F1 Championship Standings</option>
            <option value="https://www.f1-fansite.com/f1-results/2005-f1-championship-standings/">2005 F1 Championship Standings</option>
            <option value="https://www.f1-fansite.com/f1-results/2004-f1-championship-standings/">2004 F1 Championship Standings</option></select>
        </div>
        
        <script>
            
            $(document).ready(function() {
                f1results();
                
            });
            
            
            
            function f1results(){
                
              var url= $( "#resultsmenu" ).val();
             
              ajaxf1results(url);
            }
            
            
            function ajaxf1results(url){
                var driverdataset=[];
             var teamdataset=[];
              
    //  alert(dara);



        $.ajax({
             url: "f1results?url="+url,
            
            crossDomain: true,
            type: 'POST',
            // dataType: 'json',
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            async: false,
            success: function (data) {
                var tt = JSON.stringify(data,
                        null, 2)
                 
                var obj = JSON.parse(tt);
 
 
 var driver = JSON.parse(obj).driver;
 var team = JSON.parse(obj).team;
 
 
 

 



 
  for(var i in driver)
{
     var name = driver[i].name;
     var races = driver[i].races;
     var first=driver[i].first;
     var second=driver[i].second;
     var third=driver[i].third;
     var podiums=driver[i].podiums;
     var poles=driver[i].poles;
     var laps=driver[i].laps;
     var fastest=driver[i].fastest;
     var lead=driver[i].lead;
     var points = driver[i].points;
     var driv=[];
     driv[0]=name;
     driv[1]=races;
     driv[2]=first;
     driv[3]=second;
     driv[4]=third;
     driv[5]=podiums;
     driv[6]=poles;
     driv[7]=laps;
     driv[8]=fastest;
     driv[9]=lead;
     driv[10]=points;
     driverdataset[i]=driv;
     
    // alert(name +"--"+points);
}

for(var i in team)
{
     var name = team[i].name;
     var points = team[i].points;
      var tea=[];
     tea[0]=name;
     tea[1]=points
     teamdataset[i]=tea;
    // alert(name +"--"+points);
}
 


                //  alert(JSON.stringify(JSON.parse(JSON.stringify(obj1.Attributes))));
                                   //alert(share);
                    
 
                 
                

            },
            error: function (jqXHR, exception) {
                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status == 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                }
                alert(msg);
            },beforeSend: function() {   },
                complete: function() {  }
        });


 
 $('#driver').DataTable( {
        data: driverdataset,
        columns: [
            { title: "Name" },
            { title: "Races" },
            { title: "1st" },
            { title: "2nd" },
            { title: "3rd" },
            { title: "Podiums" },
            { title: "Poles" },
            { title: "Lap" },
            { title: "Fastest" },
            { title: "Lead" },
            { title: "Points" }
            
        ],
         "order": [[ 10, "desc" ]],
         "aoColumns": [{ "bSortable": false },{ "bSortable": false },{ "bSortable": false },{ "bSortable": false },{ "bSortable": false },{ "bSortable": false },{ "bSortable": false },{ "bSortable": false },{ "bSortable": false },{ "bSortable": false },{ "bSortable": true }],
          destroy: true,
          "bDestroy": true
    } );
    
     $('#team').DataTable( {
        data: teamdataset,
        columns: [
            { title: "Name" },
            { title: "Points" }
            
        ],
         "order": [[ 1, "desc" ]],
          destroy: true,
          "bDestroy": true
    } );
 
            }
            
             

        </script>
        
        <div>
            <h3>Driver Result</h3>
        </div>
        <table id="driver" class="display" width="100%"></table>
        
        
        <div>
            <h3>Team Result</h3>
        </div>
         <table id="team" class="display" width="100%"></table>
        
        <%
        
        
        
        %>
    </body>
</html>
