/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.f1.extract;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

/**
 *
 * @author prasanna
 */
public class f1results extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

            String url = "";
            url = request.getParameter("url");

            org.jsoup.nodes.Document doc = Jsoup.connect(url).get();
            JSONObject ja = new JSONObject();
            JSONArray driver = new JSONArray();
            JSONArray team = new JSONArray();
            Element drivertable = doc.select("table").get(2);
            org.jsoup.select.Elements driverrows = drivertable.select("tr");

            for (org.jsoup.nodes.Element driverrow : driverrows) {

                org.jsoup.select.Elements drivercolumns = driverrow.select("td");
                if (drivercolumns.size() > 0) {

                    String name = drivercolumns.get(0).toString().replaceAll("\\<.*?>", "");
                    String races = drivercolumns.get(1).toString().replaceAll("\\<.*?>", "");
                    String first = drivercolumns.get(2).toString().replaceAll("\\<.*?>", "");
                    String second = drivercolumns.get(3).toString().replaceAll("\\<.*?>", "");
                    String third = drivercolumns.get(4).toString().replaceAll("\\<.*?>", "");
                    String podiums = drivercolumns.get(5).toString().replaceAll("\\<.*?>", "");
                    String poles = drivercolumns.get(6).toString().replaceAll("\\<.*?>", "");
                    String laps = drivercolumns.get(7).toString().replaceAll("\\<.*?>", "");
                    String fastest = drivercolumns.get(8).toString().replaceAll("\\<.*?>", "");
                    String lead = drivercolumns.get(9).toString().replaceAll("\\<.*?>", "");
                    String points = drivercolumns.get(drivercolumns.size() - 1).toString().replaceAll("\\<.*?>", "");

                    JSONObject driverObj = new JSONObject();
                    driverObj.put("name", name);
                    driverObj.put("races", races);
                    driverObj.put("first", first);
                    driverObj.put("second", second);
                    driverObj.put("third", third);
                    driverObj.put("podiums", podiums);
                    driverObj.put("poles", poles);
                    driverObj.put("laps", laps);
                    driverObj.put("fastest", fastest);
                    driverObj.put("lead", lead);
                    driverObj.put("points", points);
                    driver.put(driverObj);

                }

            }

            Element teamtable = doc.select("table").get(3);
            org.jsoup.select.Elements teamrows = teamtable.select("tr");

            for (org.jsoup.nodes.Element teamrow : teamrows) {

                org.jsoup.select.Elements teamcolumns = teamrow.select("td");
                if (teamcolumns.size() > 0) {

                    if (teamcolumns.get(0).toString().contains("rowspan")) {

                        String name = teamcolumns.get(1).toString().replaceAll("\\<.*?>", "");
                        String points = teamcolumns.get(teamcolumns.size() - 1).toString().replaceAll("\\<.*?>", "");

                        JSONObject teamObj = new JSONObject();
                        teamObj.put("name", name);
                        teamObj.put("points", points);
                        team.put(teamObj);

                    }
                }

            }

            ja.put("driver", driver);
            ja.put("team", team);
            out.print(ja);
        } catch (java.net.MalformedURLException exception) {
            exception.printStackTrace();
        } catch (java.io.IOException exception) {
            exception.printStackTrace();
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
